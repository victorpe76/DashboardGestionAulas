# Aplicación de Gestión de aulas de ECI formacion

Aplicación web para la gestión administrativa del uso compartido y en alquiler de las aulas del centro de formación privado ECI formación de Salamanca.

## Instalacion y configuracion
Para que la aplicación funcione correctamente debe conectarse a la API desarrollada conjuntamente a este proyecto.
La url de la API debe ser establecida en el archivo src/app/app.module.ts en la variable provider BASE_PATH. Si se deja comentada la aplicación se intentará conectar a la API PENSANDO que está en la ruta relativa /api del mismo servidor.

### Ejecución en modo local
Bastará ejecutar 
```bash
$ npm install
$ npm start
```
