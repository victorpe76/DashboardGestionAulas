import { ConfiguracionRoutingModule } from './configuracion-routing.module';
import { ConfiguracionComponent } from './configuracion.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import {LoadingModule} from 'ngx-loading';
@NgModule({
  imports: [
    CommonModule,ConfiguracionRoutingModule,
    ReactiveFormsModule, FormsModule,
    LoadingModule
  ],
  declarations: [ConfiguracionComponent]
})
export class ConfiguracionModule { }
