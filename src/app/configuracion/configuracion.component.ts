import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../router.animations';
import { ConfiguracionApi } from '../api-gestion-aulas-client/index';
import { Router } from '@angular/router';

@Component({
  selector: 'app-configuracion',
  templateUrl: './configuracion.component.html',
  styleUrls: ['./configuracion.component.scss'],
  animations: [routerTransition()]
})
export class ConfiguracionComponent implements OnInit {
    loading: boolean;
    googlelink: string;
    codigo: string;
  constructor( public configuracionapi: ConfiguracionApi, private router: Router) { }

  ngOnInit() {
      this.loading=true;
    this.configuracionapi.configurargoogleGet().toPromise().then(response => {
        console.log(response);
        this.googlelink=response.googlelink;
        this.loading=false;
    }, error =>{
        console.log(error);
        if(error.status == 401 ){
            //Las credenciales ya han sido establecidas
            this.loading=false;
            localStorage.setItem('gestionaulasconfigurada', 'true');
            this.router.navigate(['/dashboard']);
        }
    });
  }

  configurarApp() {
      this.loading=true;
    console.log(this.codigo);
    this.configuracionapi.configurargooglePost(this.codigo).toPromise().then(response => {
        localStorage.setItem('gestionaulasconfigurada', 'true');
        this.loading=false;
        this.router.navigate(['/dashboard']);
    });

}
}
