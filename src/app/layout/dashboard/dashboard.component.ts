import { Component, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { CalendarComponent } from 'ng-fullcalendar';
import { Options } from 'fullcalendar';
import { AdministracionApi, Aula, AlquilerApi } from '../../api-gestion-aulas-client/index';
import 'rxjs/add/operator/toPromise';
import * as moment from 'moment';
import { Router } from '@angular/router';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    animations: [routerTransition()]
})
export class DashboardComponent implements OnInit {
    @ViewChild('caldisponibilidad') ucCalendar: CalendarComponent;

    calendarOptions: Options;
    public alerts: Array<any> = [];
    public sliders: Array<any> = [];
    public aulas: Aula[];
    public loading: boolean;

    constructor(public administracionapi: AdministracionApi,
        private router: Router,
                public alquilerapi: AlquilerApi) {
        this.sliders.push(
            {
                imagePath: 'assets/images/slider1.jpg',
                label: 'First slide label',
                text:
                    'Nulla vitae elit libero, a pharetra augue mollis interdum.'
            },
            {
                imagePath: 'assets/images/slider2.jpg',
                label: 'Second slide label',
                text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
            },
            {
                imagePath: 'assets/images/slider3.jpg',
                label: 'Third slide label',
                text:
                    'Praesent commodo cursus magna, vel scelerisque nisl consectetur.'
            }
        );

        this.alerts.push(
            {
                id: 1,
                type: 'success',
                message: `Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Voluptates est animi quibusdam praesentium quam, et perspiciatis,
                consectetur velit culpa molestias dignissimos
                voluptatum veritatis quod aliquam! Rerum placeat necessitatibus, vitae dolorum`
            },
            {
                id: 2,
                type: 'warning',
                message: `Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Voluptates est animi quibusdam praesentium quam, et perspiciatis,
                consectetur velit culpa molestias dignissimos
                voluptatum veritatis quod aliquam! Rerum placeat necessitatibus, vitae dolorum`
            }
        );
    }

    public getAulas(){
//Obtenemos las aulas;
this.administracionapi.getAulas().toPromise().then(aulas => {
    this.aulas=aulas;
   this.actualizarCalendario(this.ucCalendar.fullCalendar('getView'));
}
);
    }
    ngOnInit() {


        this.calendarOptions = {

                    buttonText:{
                                today:    'Hoy',
                                month:    'Mes',
                                week:     'Semana',
                                day:      'Dia',
                                list:     'Lista'
                            },
                    dayNamesShort:['D','L', 'M','X','J','V','S'],
                    timeFormat:'hh:mm',
                    editable: true,
                    eventLimit: false,
                    header: {
                                left:   'month,agendaWeek',
                                center: '',
                                right:  'today prev,next'
                            },
                    events:[],
                    };

        this.getAulas();
    }

    public closeAlert(alert: any) {
        const index: number = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    }

    public visitarAula(event: any){
            console.log(event);
            this.router.navigate(['/aulas/detalle',{'aula':event.event.aula}]);
            return false;
    }
    public actualizarCalendario(eventoview: any){
        this.ucCalendar.fullCalendar('removeEvents');
        this.loading=true;
        console.log(eventoview);
        let inicio=moment(eventoview.start).format('YYYY-MM-DD');
        let fin=moment(eventoview.end).format('YYYY-MM-DD');
        let cuenta = 0;
        if(this.aulas==null || this.aulas.length==0){
            this.loading=false;
            return;
        }
        for(let aula in this.aulas){

            let nombreaula=this.aulas[aula].nombre;
            let idaula=this.aulas[aula].id;
            this.alquilerapi.aulaIdaulaOcupacionDesdeHastaGet(this.aulas[aula].id,
            inicio, fin).toPromise().then( resultado => {
                console.log(resultado);
                let eventos=[];
                for(let c in resultado){
                    let evento={'title':nombreaula,
                                'aula':idaula,
                                'start':resultado[c].start,
                                'end':resultado[c].end

                    }
                    eventos.push(evento);

                }
                if(cuenta == (this.aulas.length-1)){
                    this.loading=false;
                }
                cuenta++;


                this.ucCalendar.fullCalendar('renderEvents', eventos);

            });
        }
    }
    calendarChange(evento: any){
        this.actualizarCalendario(evento.view);

    }
}
