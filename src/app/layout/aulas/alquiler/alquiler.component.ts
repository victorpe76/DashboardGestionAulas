import { AlquilerRetorno } from './../../../api-gestion-aulas-client/model/Alquiler';
import { Component, OnInit, ViewChild } from '@angular/core';
import { CalendarComponent } from 'ng-fullcalendar';
import { Options } from 'fullcalendar';
import { routerTransition } from '../../../router.animations';
import { Router, Routes, RouterModule, ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/toPromise';
import { AlquilerApi, AdministracionApi, Alquiler, Aula, Horario, Tarifa, GoogleEvent } from '../../../api-gestion-aulas-client/index';
import * as moment from 'moment';
const DIASSEMANA = ['L', 'M','X','J','V','S','D'];
@Component({
  selector: 'app-alquiler',
  templateUrl: './alquiler.component.html',
  styleUrls: ['./alquiler.component.scss'],
  animations: [routerTransition()]
})
export class AlquilerComponent implements OnInit {

  @ViewChild('caldisponibilidad') ucCalendar: CalendarComponent;

calendarOptions: Options;
  constructor(public route: ActivatedRoute,public administracion: AdministracionApi
    , private router: Router,public alquilerapi: AlquilerApi) { }

    alquiler: AlquilerRetorno;
    aula: Aula;
    nombrealquiler: string;
    totalhoras: number = 0;
    importetotal: number =0;
    loading: boolean;
    mostrardisponibilidad = false;
    eventosocupados: any[];
    tarifas: Tarifa[];
    clienteactual: string;
    nhorario = {'desde':'','hasta':'','dias': []};
    mask = [/\d/,/\d/,':', /\d/, /\d/];
  ngOnInit() {

        this.route.paramMap.switchMap( (params: ParamMap) => {
            this.cargarAula(params.get('aula'));
            return this.alquilerapi.
            getAlquilerAula(params.get('aula'),params.get('alquiler')).toPromise();
        }).subscribe(alquiler => {this.alquiler = alquiler;

            this.calendarOptions = {

                        buttonText:{
                                    today:    'Hoy',
                                    month:    'Mes',
                                    week:     'Semana',
                                    day:      'Dia',
                                    list:     'Lista'
                                },
                        dayNamesShort:['D','L', 'M','X','J','V','S'],
                        timeFormat:'hh:mm',
                        editable: false,

                        eventLimit: false,
                        header: {
                                    left:   'month,agendaWeek',
                                    center: '',
                                    right:  'today prev,next'
                                },
                        events:this.alquiler.horario,
                        };
            this.calcularDuracion(this.alquiler.horario);
        });






  }
  public calcularDuracion(eventos: Array<GoogleEvent>){
      eventos.forEach(evento => {
            let df=moment(evento.end).diff(moment(evento.start));
           this.totalhoras = this.totalhoras + moment.duration(df).asHours();

      });



      let precio = + this.aula.tarifas.find(t => t.nombre==this.alquiler.tarifa).preciohora;

      this.importetotal=this.totalhoras * precio;
    }
  public cargarAula(idaula: string){
      this.administracion.getAula(idaula).toPromise().then(aula => this.aula=aula);
  }


}
