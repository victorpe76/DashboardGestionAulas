import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { AdministracionApi } from '../../api-gestion-aulas-client/index';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'app-aulas',
  templateUrl: './aulas.component.html',
  styleUrls: ['./aulas.component.scss'],
  animations: [routerTransition()]
})
export class AulasComponent implements OnInit {
    @ViewChild('tmpl_ver') tmpl_ver: TemplateRef<any>;

    columnas = [];
    aulas = [];
    mensajes ={emptyMessage: 'No hay aulas'};

    constructor(public administracion: AdministracionApi) {


    }

    ngOnInit() {
        this.columnas = [
            { prop: 'nombre',name:'Nombre' ,flexGrow:1},
            { prop: 'capacidad',name: 'Capacidad',flexGrow:1 },
            { prop: 'equipos',name: 'Equipos',flexGrow:1 },
            { prop: 'id',name: 'Ver',cellTemplate:this.tmpl_ver,flexGrow:1 }
          ];

        this.administracion.getAulas().toPromise().then( aulas => {
          this.aulas = aulas;

        });
    }



}
