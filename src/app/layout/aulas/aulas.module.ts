import { FullCalendarModule } from 'ng-fullcalendar';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AulasComponent } from './aulas.component';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import { PageHeaderModule } from './../../shared';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { AulasRoutingModule } from './aulas-routing.module';
import {MatIconModule, MatInputModule, MatButtonModule,
    MatSelectModule, MatDatepickerModule, MatNativeDateModule, MatCheckboxModule} from '@angular/material';
import { AulaComponent } from './aula/aula.component';
import { AlquilerComponent } from './alquiler/alquiler.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import {LoadingModule} from 'ngx-loading';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
@NgModule({
    imports: [CommonModule, AulasRoutingModule, PageHeaderModule,NgxDatatableModule,
         MatIconModule,
        MatInputModule,
        MatButtonModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatSelectModule,
        MatCheckboxModule,
        FormsModule, ReactiveFormsModule,
        FullCalendarModule,
        ConfirmationPopoverModule.forRoot({
            confirmButtonType: 'danger' // set defaults here
          }),
    LoadingModule],
  declarations: [AulasComponent, AulaComponent,AlquilerComponent],
  providers:[{provide: MAT_DATE_LOCALE, useValue: 'es-ES'},
  {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
  {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},],
})
export class AulasModule { }
