import { AlquilerApi } from './../../../api-gestion-aulas-client/api/AlquilerApi';
import { Tarifa } from './../../../api-gestion-aulas-client/model/Tarifa';
import { AdministracionApi } from './../../../api-gestion-aulas-client/api/AdministracionApi';
import { Component, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { Router, Routes, RouterModule, ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/toPromise';
import { Aula } from '../../../api-gestion-aulas-client/index';
import { Options } from 'fullcalendar';
import { CalendarComponent } from 'ng-fullcalendar';
import * as moment from 'moment';
@Component({
  selector: 'app-aula',
  templateUrl: './aula.component.html',
  styleUrls: ['./aula.component.scss'],
  animations: [routerTransition()]
})
export class AulaComponent implements OnInit {
    @ViewChild('caldisponibilidad') ucCalendar: CalendarComponent;

        calendarOptions: Options;
    loading:boolean;
    aula : Aula;
    nuevatarifa :string;
    nuevatprecio : string;
    nombreaula :string="Nueva";
  constructor(private route: ActivatedRoute,public administracion: AdministracionApi,
    public alquilerapi: AlquilerApi
    ,private router: Router) {

   }

  ngOnInit() {
      this.loading=true;

    this.route.paramMap.switchMap( (params: ParamMap) => {
        this.loading=false;
        return this.administracion.getAula(params.get('aula')).toPromise();

         }).subscribe( aula => {
             if(aula !=null){
                this.aula = aula;
                this.nombreaula =this.aula.nombre;
                this.calendarOptions = {

                                        buttonText:{
                                                    today:    'Hoy',
                                                    month:    'Mes',
                                                    week:     'Semana',
                                                    day:      'Dia',
                                                    list:     'Lista'
                                                },
                                        dayNamesShort:['D','L', 'M','X','J','V','S'],
                                        timeFormat:'hh:mm',
                                        editable: true,
                                        eventLimit: false,
                                        header: {
                                                    left:   'month,agendaWeek',
                                                    center: '',
                                                    right:  'today prev,next'
                                                },
                                        events:[],
                                        };

             }else{
                this.aula = {nombre:'',capacidad:0,equipos:0,tarifas:[]};
             }

         }, error => {
            this.aula = {nombre:'',capacidad:0,equipos:0,tarifas:[]};
         });
  }

  public nuevaTarifa () {
      let tarifa: Tarifa = {nombre: this.nuevatarifa,
    preciohora: +this.nuevatprecio};
        if(this.aula.tarifas == null) {
            this.aula.tarifas = [];
        }
        this.aula.tarifas.push(tarifa);
        this.nuevatarifa='';
        this.nuevatprecio='';
  }
  public borrarTarifa (indice: number) {
        this.aula.tarifas.splice(indice,1);
  }

  public borrarAula(){
    this.loading=true;
    if(this.nombreaula!="Nueva"){
        this.administracion.delAula(this.aula.id).toPromise().then(resultado => {
            console.log(resultado);
            if(resultado != null ){
                this.loading=false;
                this.router.navigate(['/aulas']);
            }
        });
    }
  }
  public guardarAula(){
        this.loading=true;
        if(this.nombreaula!="Nueva"){
            this.administracion.editAula(this.aula.id,this.aula).toPromise().then(resultado => {
                console.log(resultado);
                if(resultado != null ){
                    this.loading=false;
                    this.router.navigate(['/aulas']);
                }
            });
        }else{
            this.administracion.addAula(this.aula).toPromise().then(resultado => {
                console.log(resultado);
                if(resultado != null ){
                    this.loading=false;
                    this.router.navigate(['/aulas']);
                }
            });
        }

  }


  public actualizarCalendario(eventoview: any){

    let inicio=moment(eventoview.start).format('YYYY-MM-DD');
    let fin=moment(eventoview.end).format('YYYY-MM-DD');


        this.alquilerapi.aulaIdaulaAlquilerDesdeHastaGet(this.aula.id,
        inicio, fin).toPromise().then( resultado => {
            console.log(resultado);
            let eventos=[];
            for(let c in resultado){

                for(let h in resultado[c].horario){
                    let evento={'title':resultado[c].ref,
                    'idalquiler':resultado[c].id,
                    'start':resultado[c].horario[h].start,
                    'end':resultado[c].horario[h].end
                };
                        eventos.push(evento);
                }

            }



            this.ucCalendar.fullCalendar('renderEvents', eventos);

        });

}

public visitarAula(event: any){
    console.log(event);
    this.router.navigate(['/aulas/alquiler',{'aula':this.aula.id,'alquiler':event.event.idalquiler}]);
    return false;
}


calendarChange(evento: any){
    this.actualizarCalendario(evento.view);

}

}
