import { AulaComponent } from './aula/aula.component';

import { AulasComponent } from './aulas.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlquilerComponent } from './alquiler/alquiler.component';

const routes: Routes = [
    {
        path: '',
        component: AulasComponent
    },
    {
        path: 'detalle',
        component: AulaComponent
    },
    {
        path: 'alquiler',
        component: AlquilerComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AulasRoutingModule {}
