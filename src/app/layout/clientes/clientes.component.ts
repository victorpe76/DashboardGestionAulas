import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { AdministracionApi } from '../../api-gestion-aulas-client/index';

import 'rxjs/add/operator/toPromise';
@Component({
    selector: 'app-clientes',
    templateUrl: './clientes.component.html',
    styleUrls: ['./clientes.component.scss'],
    animations: [routerTransition()]
})

export class ClientesComponent implements OnInit {
    @ViewChild('tmpl_simple') tmpl_simple: TemplateRef<any>;
    @ViewChild('tmpl_ver') tmpl_ver: TemplateRef<any>;
    columnas = [];
    clientes = [];
    mensajes ={emptyMessage: 'No hay clientes'};

    constructor(public administracion: AdministracionApi) {


    }

        ngOnInit() {
            this.columnas= [
                { prop: 'nombre',name:'Nombre' ,flexGrow:1 ,
                cellTemplate: this.tmpl_simple},
                { prop: 'cif',name: 'CIF',cellTemplate:this.tmpl_simple,flexGrow:1 },
                { prop: 'telefono',name: 'Teléfono',cellTemplate:this.tmpl_simple,flexGrow:1 },
                { prop: 'representante',name: 'Representante',cellTemplate:this.tmpl_simple,flexGrow:1 },
                { prop: 'email', name: 'e-mail', cellTemplate:this.tmpl_simple, flexGrow:1},
                { prop: 'id', name: 'Ver', cellTemplate:this.tmpl_ver, flexGrow:1},
              ];

            this.administracion.getClientes().toPromise().then( clientes => {
              this.clientes = clientes;
              console.log(this.clientes);
            });
        }
}
