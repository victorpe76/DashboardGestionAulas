import { ClientesComponent } from './clientes.component';
import { ClienteComponent } from './cliente/cliente.component';
import {AlquilerComponent} from './alquiler/alquiler.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {
        path: '',
        component: ClientesComponent
    },
    {
        path: 'detalle',
        component: ClienteComponent
    },
    {
        path: 'alquiler',
        component: AlquilerComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ClientesRoutingModule {}
