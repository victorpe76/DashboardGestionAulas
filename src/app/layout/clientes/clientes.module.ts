import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientesRoutingModule } from './clientes-routing.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ClientesComponent } from './clientes.component';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import { PageHeaderModule } from './../../shared';
import { ClienteComponent } from './cliente/cliente.component';
import { LoadingModule } from 'ngx-loading';
import { MatIconModule, MatInputModule, MatButtonModule,
    MatDatepickerModule, MatNativeDateModule, MatSelectModule, MatCheckboxModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AlquilerComponent } from './alquiler/alquiler.component';
import { FullCalendarModule } from 'ng-fullcalendar';
import { TextMaskModule } from 'angular2-text-mask';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
    imports: [CommonModule, ClientesRoutingModule, PageHeaderModule, NgxDatatableModule,
        MatIconModule,
        MatInputModule,
        MatButtonModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatSelectModule,
        MatCheckboxModule,
        FullCalendarModule,
        FormsModule, ReactiveFormsModule,
        ToastrModule.forRoot(),
        ConfirmationPopoverModule.forRoot({
            confirmButtonType: 'danger' // set defaults here
          }),
        TextMaskModule,
    LoadingModule],
    declarations: [ClientesComponent, ClienteComponent, AlquilerComponent],
    providers:[{provide: MAT_DATE_LOCALE, useValue: 'es-ES'},
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},],
})
export class ClientesModule { }
