import { Component, OnInit, ViewChild } from '@angular/core';
import { CalendarComponent } from 'ng-fullcalendar';
import { Options } from 'fullcalendar';
import { routerTransition } from '../../../router.animations';
import { Router, Routes, RouterModule, ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/toPromise';
import { AlquilerApi, AdministracionApi, Alquiler, Aula, Horario, Tarifa } from '../../../api-gestion-aulas-client/index';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
const DIASSEMANA = ['L', 'M','X','J','V','S','D'];
@Component({
  selector: 'app-alquiler',
  templateUrl: './alquiler.component.html',
  styleUrls: ['./alquiler.component.scss'],
  animations: [routerTransition()]
})
export class AlquilerComponent implements OnInit {

  @ViewChild('caldisponibilidad') ucCalendar: CalendarComponent;

calendarOptions: Options;
  constructor(public route: ActivatedRoute,public administracion: AdministracionApi
    , private router: Router,public alquilerapi: AlquilerApi,
    private toastr: ToastrService) { }

    alquiler: Alquiler;
    nombrealquiler: string;
    aulas: Aula[];
    loading: boolean;
    mostrardisponibilidad = false;
    eventosocupados: any[];
    tarifas: Tarifa[];
    clienteactual: string;
    nhorario = {'desde':'','hasta':'','dias': []};
    mask = [/\d/,/\d/,':', /\d/, /\d/];
  ngOnInit() {

        this.route.paramMap.subscribe((params: ParamMap) => {
            console.log(params);
            this.clienteactual = params.get('cliente');
            this.alquiler = {};
        });
        this.administracion.getAulas().toPromise().then(aulas => {
            this.aulas = aulas;
        });

        this.calendarOptions = {

        buttonText:{
                    today:    'Hoy',
                    month:    'Mes',
                    week:     'Semana',
                    day:      'Dia',
                    list:     'Lista'
                },
        dayNamesShort:['D','L', 'M','X','J','V','S'],
        timeFormat:'hh:mm',
        editable: true,
        height:300,
        eventLimit: false,
        header: {
                    left:   'month,agendaWeek',
                    center: '',
                    right:  'today prev,next'
                },
        events:[],
        };



  }

  public comprobarDisponibilidad() {
      console.log("Comprobamos disponibilidad");
      if(this.alquiler.inicio != null  && this.alquiler.fin != null
            && this.alquiler.idaula != null){
          console.log(moment(this.alquiler.inicio).format('YYYY-MM-DD'));
          this.loading = true;
          this.eventosocupados = [];

          this.alquilerapi.aulaIdaulaOcupacionDesdeHastaGet(this.alquiler.idaula,
            moment(this.alquiler.inicio).format('YYYY-MM-DD'),
            moment(this.alquiler.fin).format('YYYY-MM-DD')
        ).toPromise().then(resultado => {


            let eventos=[];
            for(let c in resultado){
                let evento={'title':'Ocupado',
                            'start':resultado[c].start,
                            'end':resultado[c].end

                }
                eventos.push(evento);
            }
            if(eventos.length > 0){
                this.mostrardisponibilidad=true;
            }else{
                this.mostrardisponibilidad=false;
            }
            console.log(eventos);
            this.loading = false;
            this.calendarOptions.events=eventos;
            this.ucCalendar.fullCalendar( 'gotoDate', eventos[0].start )

            this.ucCalendar.fullCalendar('renderEvents', eventos);
            //this.ucCalendar.fullCalendar('changeView', 'agendaWeek');

            this.ucCalendar.fullCalendar({'height':350});
            this.ucCalendar.fullCalendar('render');

        });
      }

  }

  public eliminarHorario(indice: number){
      this.alquiler.horarios.splice(indice,1);
  }

  public incluirHorario() {
      let horario = {'desde':this.nhorario.desde+':00','hasta':this.nhorario.hasta+':00',
    'recurrencia':[]
};
        for(let i =0; i < 7 ;i++) {
            if(this.nhorario.dias[i] == true) {
                horario.recurrencia.push(i);
            }
        }
        if(this.alquiler.horarios == null) {
            this.alquiler.horarios = [];
        }
        this.alquiler.horarios.push(horario);
        console.log(this.alquiler);
        this.nhorario = {'desde': '', 'hasta': '', 'dias': []};
  }

  public generarDias(recurrencia: Array<number>) : string{
      let retorno='';
      for(let r in recurrencia){
        retorno=retorno+DIASSEMANA[r]+',';
      }
      return retorno.substr(0,retorno.length-1);
  }

    public ofrecerTarifas(){
        this.tarifas=this.aulas.find(a => a.id==this.alquiler.idaula).tarifas;

    }
  public crearAlquiler(){
      if(this.alquiler.horarios==null || this.alquiler.horarios.length==0){
        this.toastr.error('No se han creado horarios para ester alquiler','Error al guardar',{
            timeOut:1500
        });
        return;
      }
        this.loading=true;
        this.alquiler.inicio=moment(this.alquiler.inicio).format('YYYY-MM-DD');
        this.alquiler.fin=moment(this.alquiler.fin).format('YYYY-MM-DD');
        this.alquilerapi.altaAlquiler(this.clienteactual,this.alquiler).toPromise().then(response => {
            console.log(response);
            this.loading=false;
            this.router.navigate(['/clientes/detalle',{cliente:this.clienteactual}]);
        });
  }

}
