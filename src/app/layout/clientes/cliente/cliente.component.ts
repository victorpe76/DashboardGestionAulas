
import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { Cliente, AdministracionApi, AlquilerApi, Alquiler } from '../../../api-gestion-aulas-client/index';
import { routerTransition } from '../../../router.animations';
import { Router, Routes, RouterModule, ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/toPromise';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.scss'],
  animations: [routerTransition()]
})
export class ClienteComponent implements OnInit {
    @ViewChild('tmpl_ver') tmpl_ver: TemplateRef<any>;
    loading:boolean;
    cliente: Cliente;
    columnas = [];
    alquileres = [];
    mensajes ={emptyMessage: 'No hay alquileres'};
    nombrecliente="Nuevo";
  constructor(private route: ActivatedRoute,public administracion: AdministracionApi
    ,private router: Router,public alquiler: AlquilerApi,
    public toastr: ToastrService) { }

  ngOnInit() {
    this.loading=true;
    this.route.paramMap.switchMap( (params: ParamMap) => {
        this.loading=false;
        return this.administracion.getCliente(params.get('cliente')).toPromise();

         }).subscribe( cliente => {
             if(cliente !=null){
                this.cliente = cliente;
                this.nombrecliente =this.cliente.nombre;
                this.cargarAlquileres();
             }else{
                this.cliente = {};
             }

         }, error => {
            this.cliente = {nombre:''};
         });
         this.columnas= [
            { prop: 'ref',name:'Referencia' ,flexGrow:1},
            { prop: 'inicio',name: 'Desde',flexGrow:1 },
            { prop: 'fin',name: 'Hasta',flexGrow:1 },
            { prop: 'id', name: '', cellTemplate:this.tmpl_ver, flexGrow:1},
          ];

  }

  public borrarCliente(){
    this.loading=true;
    if(this.nombrecliente!="Nuevo"){
        this.administracion.delCliente(this.cliente.id).toPromise().then(resultado => {
            console.log(resultado);
            if(resultado != null ){
                this.loading=false;
                this.router.navigate(['/clientes']);
            }
        });
    }
  }
  public guardarCliente(){

    //Comprobamos campos
    if(this.cliente.cif == null || this.cliente.nombre == null
    || this.cliente.representante == null || this.cliente.email == null ||
    this.cliente.telefono == null){
        this.toastr.error('Algunos campos están vacíos','Error al guardar',{timeOut:750});
        return;
    }
    this.loading=true;
    if(this.nombrecliente!="Nuevo"){
        this.administracion.editCliente(this.cliente.id,this.cliente).toPromise().then(resultado => {
            console.log(resultado);
            if(resultado != null ){
                this.loading=false;
                this.router.navigate(['/clientes']);
            }
        });
    }else{
        this.administracion.addCliente(this.cliente).toPromise().then(resultado => {
            console.log(resultado);
            if(resultado != null ){
                this.loading=false;
                this.router.navigate(['/clientes']);
            }
        });
    }
  }

  cargarAlquileres(){
      this.loading=true;
      this.alquiler.getAlquileresByCliente(this.cliente.id).toPromise().then(alquileres => {
        this.alquileres=alquileres;
        this.loading=false;
        console.log(this.alquileres);
      });
  }
  borrarAlquiler(id: string){
      this.loading=true;
        this.alquiler.delAlquiler(this.cliente.id,id).toPromise().then(resultado => {
                console.log(resultado);
                this.loading=false;
                this.cargarAlquileres();
        });
  }
  public verAlquiler(aula: any){

    this.router.navigate(['/aulas/alquiler',{'aula':aula.aula.id,'alquiler':aula.id}]);
    return false;
}
}
