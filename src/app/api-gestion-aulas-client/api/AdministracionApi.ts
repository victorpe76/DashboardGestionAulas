/**
 * API App Gestion de Aulas de ECI
 * API para la aplicacion de gestion de aulas de ECI FORMACION
 *
 * OpenAPI spec version: 0.0.1
 * Contact: victor@aidiapp.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Inject, Injectable, Optional }                      from '@angular/core';

import { HttpModule, Http, Headers, URLSearchParams }        from '@angular/http';
import { RequestMethod, RequestOptions, RequestOptionsArgs } from '@angular/http';
import { Response, ResponseContentType }                     from '@angular/http';

import { Observable }                                        from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import * as models                                           from '../model/models';
import { BASE_PATH }                                         from '../variables';
import { Configuration }                                     from '../configuration';
import {PlatformLocation } from '@angular/common';
/* tslint:disable:no-unused-variable member-ordering */


@Injectable()
export class AdministracionApi {
    protected basePath = 'http://localhost:8888/gestionaulas/api';
    public defaultHeaders: Headers = new Headers();
    public configuration: Configuration = new Configuration();

    constructor(private http: Http,
        @Optional()@Inject(BASE_PATH) basePath: string,
         @Optional() configuration: Configuration,
         @Optional() platformLocation: PlatformLocation
        ) {
       // console.log("El base path es "+basePath);
       // console.log((platformLocation as any).location);

        if (basePath) {
            this.basePath = basePath;
        }else{
            this.basePath=(platformLocation as any).location.href+'/../api';
        }
        if (configuration) {
            this.configuration = configuration;
        }
    }



    /**
     * Crea un nuevo aula
     * Crea un nuevo aula en el sistema para poder asignar alquileres
     * @param aula Aula a dar de alta
     */
    public addAula(aula?: models.Aula, extraHttpRequestParams?: any): Observable<{}> {
        return this.addAulaWithHttpInfo(aula, extraHttpRequestParams)
            .map((response: Response) => {
                if (response.status === 204) {
                    return undefined;
                } else {
                    return response.json();
                }
            });
    }

    /**
     * Crea un nuevo cliente
     * Crea un nuevo cliente en el sistema para poder asignar alquileres
     * @param cliente Cliente a dar de alta
     */
    public addCliente(cliente?: models.Cliente, extraHttpRequestParams?: any): Observable<{}> {
        return this.addClienteWithHttpInfo(cliente, extraHttpRequestParams)
            .map((response: Response) => {
                if (response.status === 204) {
                    return undefined;
                } else {
                    return response.json();
                }
            });
    }

    /**
     * Elimina el Aula
     * Elimina el aula especificado
     * @param idaula uuid del aula que se quiere editar
     */
    public delAula(idaula: string, extraHttpRequestParams?: any): Observable<{}> {
        return this.delAulaWithHttpInfo(idaula, extraHttpRequestParams)
            .map((response: Response) => {
                if (response.status === 204) {
                    return undefined;
                } else {
                    return response.json();
                }
            });
    }

    /**
     * Elimina el cliente
     * Elimina el cliente especificado
     * @param idcliente uuid del cliente que se quiere editar
     */
    public delCliente(idcliente: string, extraHttpRequestParams?: any): Observable<{}> {
        return this.delClienteWithHttpInfo(idcliente, extraHttpRequestParams)
            .map((response: Response) => {
                if (response.status === 204) {
                    return undefined;
                } else {
                    return response.json();
                }
            });
    }

    /**
     * Modifica el aula
     * Modifica los datos del aula especificado
     * @param idaula uuid del aula que se quiere editar
     * @param cliente Aula a modificar
     */
    public editAula(idaula: string, cliente?: models.Aula, extraHttpRequestParams?: any): Observable<{}> {
        return this.editAulaWithHttpInfo(idaula, cliente, extraHttpRequestParams)
            .map((response: Response) => {
                if (response.status === 204) {
                    return undefined;
                } else {
                    return response.json();
                }
            });
    }

    /**
     * Modifica el cliente
     * Modifica los datos del cliente especificado
     * @param idcliente uuid del cliente que se quiere editar
     * @param cliente Cliente a modificar
     */
    public editCliente(idcliente: string, cliente?: models.Cliente, extraHttpRequestParams?: any): Observable<{}> {
        return this.editClienteWithHttpInfo(idcliente, cliente, extraHttpRequestParams)
            .map((response: Response) => {
                if (response.status === 204) {
                    return undefined;
                } else {
                    return response.json();
                }
            });
    }

    /**
     * Datos de aula
     * Devuelve los datos del aula especificada
     * @param idaula uuid del aula que se quiere editar
     */
    public getAula(idaula: string, extraHttpRequestParams?: any): Observable<models.Aula> {
        return this.getAulaWithHttpInfo(idaula, extraHttpRequestParams)
            .map((response: Response) => {
                if (response.status === 204) {
                    return undefined;
                } else {
                    return response.json();
                }
            });
    }

    /**
     * Listado de Aulas
     * Devuelve el listado de aulas completo dado de alta en la aplicación
     */
    public getAulas(extraHttpRequestParams?: any): Observable<Array<models.Aula>> {
        return this.getAulasWithHttpInfo(extraHttpRequestParams)
            .map((response: Response) => {
                if (response.status === 204) {
                    return undefined;
                } else {
                    return response.json();
                }
            });
    }

    /**
     * Datos de cliente
     * Devuelve los datos del cliente especificado
     * @param idcliente uuid del cliente que se quiere editar
     */
    public getCliente(idcliente: string, extraHttpRequestParams?: any): Observable<models.Cliente> {
        return this.getClienteWithHttpInfo(idcliente, extraHttpRequestParams)
            .map((response: Response) => {
                if (response.status === 204) {
                    return undefined;
                } else {
                    return response.json();
                }
            });
    }

    /**
     * Listado de clientes
     * Devuelve el listado de clientes completo dado de alta en la aplicación
     */
    public getClientes(extraHttpRequestParams?: any): Observable<Array<models.Cliente>> {
        return this.getClientesWithHttpInfo(extraHttpRequestParams)
            .map((response: Response) => {
                if (response.status === 204) {
                    return undefined;
                } else {
                    return response.json();
                }
            });
    }


    /**
     * Crea un nuevo aula
     * Crea un nuevo aula en el sistema para poder asignar alquileres
     * @param aula Aula a dar de alta
     */
    public addAulaWithHttpInfo(aula?: models.Aula, extraHttpRequestParams?: any): Observable<Response> {
        const path = this.basePath + `/aula`;

        let queryParameters = new URLSearchParams();
        let headers = new Headers(this.defaultHeaders.toJSON()); // https://github.com/angular/angular/issues/6845


        // to determine the Content-Type header
        let consumes: string[] = [
            'application/json'
        ];

        // to determine the Accept header
        let produces: string[] = [
            'application/json'
        ];



        headers.set('Content-Type', 'application/json');


        let requestOptions: RequestOptionsArgs = new RequestOptions({
            method: RequestMethod.Post,
            headers: headers,
            body: aula == null ? '' : JSON.stringify(aula), // https://github.com/angular/angular/issues/10612
            search: queryParameters
        });

        // https://github.com/swagger-api/swagger-codegen/issues/4037
        if (extraHttpRequestParams) {
            requestOptions = Object.assign(requestOptions, extraHttpRequestParams);
        }

        return this.http.request(path, requestOptions);
    }



    /**
     * Crea un nuevo cliente
     * Crea un nuevo cliente en el sistema para poder asignar alquileres
     * @param cliente Cliente a dar de alta
     */
    public addClienteWithHttpInfo(cliente?: models.Cliente, extraHttpRequestParams?: any): Observable<Response> {
        const path = this.basePath + `/cliente`;

        let queryParameters = new URLSearchParams();
        let headers = new Headers(this.defaultHeaders.toJSON()); // https://github.com/angular/angular/issues/6845


        // to determine the Content-Type header
        let consumes: string[] = [
            'application/json'
        ];

        // to determine the Accept header
        let produces: string[] = [
            'application/json'
        ];



        headers.set('Content-Type', 'application/json');


        let requestOptions: RequestOptionsArgs = new RequestOptions({
            method: RequestMethod.Post,
            headers: headers,
            body: cliente == null ? '' : JSON.stringify(cliente), // https://github.com/angular/angular/issues/10612
            search: queryParameters
        });

        // https://github.com/swagger-api/swagger-codegen/issues/4037
        if (extraHttpRequestParams) {
            requestOptions = Object.assign(requestOptions, extraHttpRequestParams);
        }

        return this.http.request(path, requestOptions);
    }

    /**
     * Elimina el Aula
     * Elimina el aula especificado
     * @param idaula uuid del aula que se quiere editar
     */
    public delAulaWithHttpInfo(idaula: string, extraHttpRequestParams?: any): Observable<Response> {
        const path = this.basePath + `/aula/${idaula}`;

        let queryParameters = new URLSearchParams();
        let headers = new Headers(this.defaultHeaders.toJSON()); // https://github.com/angular/angular/issues/6845
        // verify required parameter 'idaula' is not null or undefined
        if (idaula === null || idaula === undefined) {
            throw new Error('Required parameter idaula was null or undefined when calling delAula.');
        }


        // to determine the Content-Type header
        let consumes: string[] = [
        ];

        // to determine the Accept header
        let produces: string[] = [
            'application/json'
        ];





        let requestOptions: RequestOptionsArgs = new RequestOptions({
            method: RequestMethod.Delete,
            headers: headers,
            search: queryParameters
        });

        // https://github.com/swagger-api/swagger-codegen/issues/4037
        if (extraHttpRequestParams) {
            requestOptions =Object.assign(requestOptions, extraHttpRequestParams);
        }

        return this.http.request(path, requestOptions);
    }

    /**
     * Elimina el cliente
     * Elimina el cliente especificado
     * @param idcliente uuid del cliente que se quiere editar
     */
    public delClienteWithHttpInfo(idcliente: string, extraHttpRequestParams?: any): Observable<Response> {
        const path = this.basePath + `/cliente/${idcliente}`;

        let queryParameters = new URLSearchParams();
        let headers = new Headers(this.defaultHeaders.toJSON()); // https://github.com/angular/angular/issues/6845
        // verify required parameter 'idcliente' is not null or undefined
        if (idcliente === null || idcliente === undefined) {
            throw new Error('Required parameter idcliente was null or undefined when calling delCliente.');
        }


        // to determine the Content-Type header
        let consumes: string[] = [
        ];

        // to determine the Accept header
        let produces: string[] = [
            'application/json'
        ];





        let requestOptions: RequestOptionsArgs = new RequestOptions({
            method: RequestMethod.Delete,
            headers: headers,
            search: queryParameters
        });

        // https://github.com/swagger-api/swagger-codegen/issues/4037
        if (extraHttpRequestParams) {
            requestOptions = Object.assign(requestOptions, extraHttpRequestParams);
        }

        return this.http.request(path, requestOptions);
    }

    /**
     * Modifica el aula
     * Modifica los datos del aula especificado
     * @param idaula uuid del aula que se quiere editar
     * @param cliente Aula a modificar
     */
    public editAulaWithHttpInfo(idaula: string, cliente?: models.Aula, extraHttpRequestParams?: any): Observable<Response> {
        const path = this.basePath + `/aula/${idaula}`;

        let queryParameters = new URLSearchParams();
        let headers = new Headers(this.defaultHeaders.toJSON()); // https://github.com/angular/angular/issues/6845
        // verify required parameter 'idaula' is not null or undefined
        if (idaula === null || idaula === undefined) {
            throw new Error('Required parameter idaula was null or undefined when calling editAula.');
        }


        // to determine the Content-Type header
        let consumes: string[] = [
            'application/json'
        ];

        // to determine the Accept header
        let produces: string[] = [
            'application/json'
        ];



        headers.set('Content-Type', 'application/json');


        let requestOptions: RequestOptionsArgs = new RequestOptions({
            method: RequestMethod.Put,
            headers: headers,
            body: cliente == null ? '' : JSON.stringify(cliente), // https://github.com/angular/angular/issues/10612
            search: queryParameters
        });

        console.log(requestOptions.body);
        // https://github.com/swagger-api/swagger-codegen/issues/4037
        if (extraHttpRequestParams) {
            requestOptions = Object.assign(requestOptions, extraHttpRequestParams);
        }

        return this.http.request(path, requestOptions);
    }

    /**
     * Modifica el cliente
     * Modifica los datos del cliente especificado
     * @param idcliente uuid del cliente que se quiere editar
     * @param cliente Cliente a modificar
     */
    public editClienteWithHttpInfo(idcliente: string, cliente?: models.Cliente, extraHttpRequestParams?: any): Observable<Response> {
        const path = this.basePath + `/cliente/${idcliente}`;

        let queryParameters = new URLSearchParams();
        let headers = new Headers(this.defaultHeaders.toJSON()); // https://github.com/angular/angular/issues/6845
        // verify required parameter 'idcliente' is not null or undefined
        if (idcliente === null || idcliente === undefined) {
            throw new Error('Required parameter idcliente was null or undefined when calling editCliente.');
        }


        // to determine the Content-Type header
        let consumes: string[] = [
            'application/json'
        ];

        // to determine the Accept header
        let produces: string[] = [
            'application/json'
        ];



        headers.set('Content-Type', 'application/json');


        let requestOptions: RequestOptionsArgs = new RequestOptions({
            method: RequestMethod.Put,
            headers: headers,
            body: cliente == null ? '' : JSON.stringify(cliente), // https://github.com/angular/angular/issues/10612
            search: queryParameters
        });

        // https://github.com/swagger-api/swagger-codegen/issues/4037
        if (extraHttpRequestParams) {
            requestOptions = Object.assign(requestOptions, extraHttpRequestParams);
        }

        return this.http.request(path, requestOptions);
    }

    /**
     * Datos de aula
     * Devuelve los datos del aula especificada
     * @param idaula uuid del aula que se quiere editar
     */
    public getAulaWithHttpInfo(idaula: string, extraHttpRequestParams?: any): Observable<Response> {
        const path = this.basePath + `/aula/${idaula}`;

        let queryParameters = new URLSearchParams();
        let headers = new Headers(this.defaultHeaders.toJSON()); // https://github.com/angular/angular/issues/6845
        // verify required parameter 'idaula' is not null or undefined
        if (idaula === null || idaula === undefined) {
            throw new Error('Required parameter idaula was null or undefined when calling getAula.');
        }


        // to determine the Content-Type header
        let consumes: string[] = [
        ];

        // to determine the Accept header
        let produces: string[] = [
            'application/json'
        ];





        let requestOptions: RequestOptionsArgs = new RequestOptions({
            method: RequestMethod.Get,
            headers: headers,
            search: queryParameters
        });

        // https://github.com/swagger-api/swagger-codegen/issues/4037
        if (extraHttpRequestParams) {
            requestOptions = Object.assign(requestOptions, extraHttpRequestParams);
        }

        return this.http.request(path, requestOptions);
    }

    /**
     * Listado de Aulas
     * Devuelve el listado de aulas completo dado de alta en la aplicación
     */
    public getAulasWithHttpInfo(extraHttpRequestParams?: any): Observable<Response> {
        const path = this.basePath + `/aula`;

        let queryParameters = new URLSearchParams();
        let headers = new Headers(this.defaultHeaders.toJSON()); // https://github.com/angular/angular/issues/6845


        // to determine the Content-Type header
        let consumes: string[] = [
        ];

        // to determine the Accept header
        let produces: string[] = [
            'application/json'
        ];





        let requestOptions: RequestOptionsArgs = new RequestOptions({
            method: RequestMethod.Get,
            headers: headers,
            search: queryParameters
        });

        // https://github.com/swagger-api/swagger-codegen/issues/4037
        if (extraHttpRequestParams) {
            requestOptions = Object.assign(requestOptions, extraHttpRequestParams);
        }

        return this.http.request(path, requestOptions);
    }

    /**
     * Datos de cliente
     * Devuelve los datos del cliente especificado
     * @param idcliente uuid del cliente que se quiere editar
     */
    public getClienteWithHttpInfo(idcliente: string, extraHttpRequestParams?: any): Observable<Response> {
        const path = this.basePath + `/cliente/${idcliente}`;

        let queryParameters = new URLSearchParams();
        let headers = new Headers(this.defaultHeaders.toJSON()); // https://github.com/angular/angular/issues/6845
        // verify required parameter 'idcliente' is not null or undefined
        if (idcliente === null || idcliente === undefined) {
            throw new Error('Required parameter idcliente was null or undefined when calling getCliente.');
        }


        // to determine the Content-Type header
        let consumes: string[] = [
        ];

        // to determine the Accept header
        let produces: string[] = [
            'application/json'
        ];





        let requestOptions: RequestOptionsArgs = new RequestOptions({
            method: RequestMethod.Get,
            headers: headers,
            search: queryParameters
        });

        // https://github.com/swagger-api/swagger-codegen/issues/4037
        if (extraHttpRequestParams) {
            requestOptions = Object.assign(requestOptions, extraHttpRequestParams);
        }

        return this.http.request(path, requestOptions);
    }

    /**
     * Listado de clientes
     * Devuelve el listado de clientes completo dado de alta en la aplicación
     */
    public getClientesWithHttpInfo(extraHttpRequestParams?: any): Observable<Response> {
        const path = this.basePath + `/cliente`;

        let queryParameters = new URLSearchParams();
        let headers = new Headers(this.defaultHeaders.toJSON()); // https://github.com/angular/angular/issues/6845


        // to determine the Content-Type header
        let consumes: string[] = [
        ];

        // to determine the Accept header
        let produces: string[] = [
            'application/json'
        ];





        let requestOptions: RequestOptionsArgs = new RequestOptions({
            method: RequestMethod.Get,
            headers: headers,
            search: queryParameters
        });

        // https://github.com/swagger-api/swagger-codegen/issues/4037
        if (extraHttpRequestParams) {
            requestOptions = Object.assign(requestOptions, extraHttpRequestParams);
        }

        return this.http.request(path, requestOptions);
    }

}
