import { TestBed, inject } from '@angular/core/testing';

import { ApiGestionAulasService } from './api-gestion-aulas.service';

describe('ApiGestionAulasService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApiGestionAulasService]
    });
  });

  it('should be created', inject([ApiGestionAulasService], (service: ApiGestionAulasService) => {
    expect(service).toBeTruthy();
  }));
});
